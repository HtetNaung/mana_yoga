var portraitWidth, landscapeWidth;
    $(window).bind("resize", function() {
      if (Math.abs(window.orientation) === 0) {
        if (/Android/.test(window.navigator.userAgent)) {
          if (!portraitWidth)
            portraitWidth = $(window).width();
        } else {
          portraitWidth = $(window).width();
        }
        $("html").css("zoom", portraitWidth / 798);
      } else {
        if (/Android/.test(window.navigator.userAgent)) {
          if (!landscapeWidth)
            landscapeWidth = $(window).width();
        } else {
          landscapeWidth = $(window).width();
        }
        $("html").css("zoom", landscapeWidth / 798);
      }
    }).trigger("resize");

// menu  //
//
//
//
$(document).ready(function(){$("a").bind("focus",function(){if(this.blur)this.blur();});});

// バーガーメニュー

$(function(){
  $(".test").on("click", function() {
    $(".g-nav").slideToggle('fade', '', 800);
   $(".panel-btn").toggleClass('active');
  });
});

$(document).ready(function() {
   
    $('.autoplay').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 5000,
      draggable: false,
    });
});
